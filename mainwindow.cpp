#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QDesktopServices>
#include <QAudio>
#include <QAudioDeviceInfo>
#include <QAVCodec>
#include <QAVFormat>
#include <QAVOutputFormat>
#include <QAVInputFormat>
#include <QHostInfo>
#include <QNetworkInterface>
#include <QMessageBox>

#if QT_VERSION < 0x050000
# define StandardPaths QDesktopServices
# define storageLocation(X) QDesktopServices::storageLocation(X)
#else
# define StandardPaths QStandardPaths
# define storageLocation(X) QStandardPaths::standardLocations(X).first()
#endif

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),	
	ui(new Ui::MainWindow),
	mediaFile(nullptr),
	rtp(nullptr),
	rtcp(nullptr)
{
	ui->setupUi(this);

	QPayloadType::initialize();
	QAVFormat::networkInit();

	inputDevices = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
	for (QList<QAudioDeviceInfo>::ConstIterator it=inputDevices.constBegin(); it!=inputDevices.constEnd(); ++it)
		ui->cmbInputDevice->addItem((*it).deviceName());
	ui->cmbInputDevice->setCurrentIndex(ui->cmbInputDevice->findText(QAudioDeviceInfo::defaultInputDevice().deviceName()));

	outputDevices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
	for (QList<QAudioDeviceInfo>::ConstIterator it=outputDevices.constBegin(); it!=outputDevices.constEnd(); ++it)
		ui->cmbOutputDevice->addItem((*it).deviceName());
	ui->cmbOutputDevice->setCurrentIndex(ui->cmbOutputDevice->findText(QAudioDeviceInfo::defaultOutputDevice().deviceName()));

	QDir outputDir(storageLocation(StandardPaths::DocumentsLocation));

	mediaFile = new QFile(outputDir.absoluteFilePath("output.wav"));

	for (QAVOutputFormat::Iterator it; it; ++it)
		ui->cmbOutputFormat->addItem((*it).longName(), (*it).name());

	ui->cmbInputFormat->addItem(tr("Default"), QString());
	for (QAVInputFormat::Iterator it; it; ++it)
		ui->cmbInputFormat->addItem((*it).longName(), (*it).name());

	QList<QHostAddress> addresses = QNetworkInterface::allAddresses();

	for (QList<QHostAddress>::ConstIterator it = addresses.constBegin();
		 it!=addresses.constEnd(); ++it)
		ui->cmbIpLocal->addItem((*it).toString());

	ui->pbCast->setDisabled(true);
}

MainWindow::~MainWindow()
{
	delete ui;
}

QAVOutputFormat MainWindow::formatByName(const QString &AName) const
{
	if (!AName.isNull())
		for (QAVOutputFormat::Iterator it; it; ++it)
			if ((*it).name()==AName)
				return *it;
	return QAVOutputFormat();
}

void MainWindow::checkRtp()
{
	ui->gpbRtp->setEnabled(ui->cmbInputFormat->itemData(ui->cmbInputFormat->currentIndex()).toString()=="rtp" ||
						   ui->cmbOutputFormat->itemData(ui->cmbOutputFormat->currentIndex()).toString()=="rtp");
}

void MainWindow::checkCast()
{
	ui->pbCast->setEnabled(ui->cmbOutputFormat->itemData(ui->cmbOutputFormat->currentIndex()).toString()=="rtp" &&
						   !ui->ledIpTarget->text().isEmpty());
}

void MainWindow::checkPlay()
{
	ui->pbPlay->setEnabled(true);
}

void MainWindow::checkRecord()
{
	ui->pbRecord->setEnabled(formatByName(ui->cmbOutputFormat->itemData(ui->cmbOutputFormat->currentIndex()).toString()) &&
							 QAVCodec::findEncoder(ui->cmbCodec->itemData(ui->cmbCodec->currentIndex()).toInt()));
}

void MainWindow::onFileSelect()
{
	bool output = qobject_cast<QAction*>(sender()) == ui->action_Output_file;
	QString outputFileName = output?QFileDialog::getSaveFileName(this, tr("Select file to write"), mediaFile->fileName())
								   :QFileDialog::getOpenFileName(this, tr("Select file to read"), mediaFile->fileName());
	if (mediaFile->fileName() != outputFileName)
		mediaFile->setFileName(outputFileName);
}

void MainWindow::onRecord()
{
	ui->pbCast->setDisabled(true);
	ui->pbPlay->setDisabled(true);
	ui->pbRecord->setDisabled(true);
	if (mediaStreamer)
		mediaStreamer->setStatus(MediaStreamer::Stopped);
	else
	{
		mediaFile->open(QIODevice::WriteOnly);
		QAVOutputFormat format = formatByName(ui->cmbOutputFormat->itemData(ui->cmbOutputFormat->currentIndex()).toString());
		if (format)
		{
			QAVCodec encoder = QAVCodec::findEncoder(ui->cmbCodec->itemData(ui->cmbCodec->currentIndex()).toInt());
			if (encoder)
			{				
				mediaStreamer =  new MediaStreamer(inputDevices.at(ui->cmbInputDevice->currentIndex()), encoder, mediaFile, format, ui->cmbSampleRate->currentText().toInt(), ui->spbBitRate->value());
				connect(mediaStreamer, SIGNAL(statusChanged(int)), SLOT(onStreamerStatusChanged(int)));
				mediaStreamer->setStatus(MediaStreamer::Running);
			}
			else
				qWarning() << "Codec not found!";
		}
		else
			qWarning() << "Format not found!";
	}
}

void MainWindow::onCast()
{
	qDebug() << "onCast()";
	ui->pbRecord->setDisabled(true);
	ui->pbPlay->setDisabled(true);
	ui->pbCast->setDisabled(true);
	if (mediaStreamer)
		mediaStreamer->setStatus(MediaStreamer::Stopped);
	else
		QHostInfo::lookupHost(ui->ledIpTarget->text(), this,
							  SLOT(onHostLookupFinished(QHostInfo)));
}

void MainWindow::onPlay()
{
	ui->pbCast->setDisabled(true);
	ui->pbRecord->setDisabled(true);
	ui->pbPlay->setDisabled(true);
	if (mediaPlayer)
		mediaPlayer->setStatus(MediaPlayer::Finished);
	else
	{
		QString formatName = ui->cmbInputFormat->itemData(ui->cmbInputFormat->currentIndex()).toString();
		if (formatName=="rtp")
		{
			QHostAddress address(ui->cmbIpLocal->currentText());
			quint16 port = static_cast<quint16>(ui->spbPortLocal->value());
			int ptId = ui->cmbPayloadType->currentText().toInt();
			QPayloadType pt = QPayloadType::staticPayloadType(ptId);
			mediaPlayer =  new MediaPlayer(outputDevices.at(ui->cmbOutputDevice->currentIndex()),
										   address, port, pt, this);
		}
		else
		{
			if (mediaFile->isOpen())
				mediaFile->close();
			mediaFile->open(QIODevice::ReadOnly);
			mediaPlayer =  new MediaPlayer(outputDevices.at(ui->cmbOutputDevice->currentIndex()),
										   mediaFile, this);
		}
		connect(mediaPlayer, SIGNAL(statusChanged(int,int)), SLOT(onPlayerStatusChanged(int,int)));
		mediaPlayer->setStatus(MediaPlayer::Running);
	}
}

void MainWindow::onDeviceComboBoxIndexChanged(int AIndex)
{
	qDebug() << "MainWindow::onDeviceComboBoxIndexChanged(" << AIndex << ")";
}

void MainWindow::onOutputFormatComboBoxIndexChanged(int AIndex)
{
	QAVOutputFormat format = formatByName(ui->cmbOutputFormat->itemData(AIndex).toString());
	ui->cmbCodec->clear();
	if (format)
	{
		if (format.name()!="rtp")			// For Non-RTP
			ui->cmbCodec->addItem(tr("Default"), QString());

		QSet<QString> supportedPt;

		QList<QAVCodec> encoders = QAVCodec::getCodecs(QAVCodec::Encoder, QAVCodec::MT_Audio);		
		for (QList<QAVCodec>::ConstIterator it=encoders.constBegin(); it!=encoders.constEnd();
			 ++it) {
			if (format.queryCodec(*it))
			{
				if (format.name()=="rtp")	// For RTP
				{
					QString ptname = QPayloadType::nameById((*it).id());

					if (ptname.isEmpty()) // Skip unnamed (unsupported) codecs
						continue;
					supportedPt.insert(ptname);
				}
				ui->cmbCodec->addItem((*it).longName(), (*it).id());
			}
		}

		if (format.name()=="rtp") {
			QMap<int, QPayloadType> payloadTypes = QPayloadType::staticPayloadTypes(true);
			for (QMap<int, QPayloadType>::ConstIterator it=payloadTypes.constBegin();
				 it!=payloadTypes.constEnd(); ++it)
				if (it->media.testFlag(QPayloadType::Audio) &&
					supportedPt.contains(it->name)) // Video not supported at the moment
					ui->cmbPayloadType->addItem(QString::number(it.key()), it.key());
		} else
			ui->cmbPayloadType->clear();

		ui->lblOutputFormatName->setText(format.name());
	}
	else
		ui->lblOutputFormatName->clear();
	checkRecord();
	checkRtp();
	checkCast();
}

void MainWindow::onOutputFormatComboBoxActivated(int AIndex)
{
	int index = ui->cmbInputFormat->findData(ui->cmbOutputFormat->itemData(AIndex));
	if (index!=-1)
		ui->cmbInputFormat->setCurrentIndex(index);
}

void MainWindow::onInputFormatComboBoxIndexChanged(int AIndex)
{
	QAVInputFormat format = QAVInputFormat::find(ui->cmbInputFormat->itemData(AIndex).toString());
	if (format)
		ui->lblInputFormatName->setText(format.name());
	else
		ui->lblInputFormatName->clear();
	checkRtp();
}

void MainWindow::onInputFormatComboBoxActivated(int AIndex)
{
	int index = ui->cmbOutputFormat->findData(ui->cmbInputFormat->itemData(AIndex));
	if (index!=-1)
		ui->cmbOutputFormat->setCurrentIndex(index);
}

void MainWindow::onCodecComboBoxIndexChanged(int AIndex)
{
	QString text;
	int id = ui->cmbCodec->itemData(AIndex).toInt();
	if (id)
	{
		text.append(QString::number(id));
		text.append('(');
		QAVCodec decoder(QAVCodec::findDecoder(id));
		if (decoder)
			text.append(decoder.name());
		text.append('/');
		QAVCodec encoder(QAVCodec::findEncoder(id));
		if (encoder)
			text.append(encoder.name());
		text.append(')');

		QList<int> supportedSampleRates = encoder.supportedSampleRates();
		ui->cmbSampleRate->clear();
		for (QList<int>::ConstIterator it=supportedSampleRates.constBegin(); it!=supportedSampleRates.constEnd(); ++it)
			ui->cmbSampleRate->addItem(QString::number(*it), *it);
		ui->cmbSampleRate->setEditable(supportedSampleRates.isEmpty());
	}
	ui->lblCodecName->setText(text);
	checkRecord();
}

void MainWindow::onHostLookupFinished(const QHostInfo &AHostInfo)
{
	if (AHostInfo.error()==QHostInfo::NoError)
	{
		QList<QHostAddress> addresses = AHostInfo.addresses();
		if (!addresses.isEmpty())
		{
			hostAddress = addresses.first();
			if (!hostAddress.isNull())
				ui->pbCast->setEnabled(true);
		}
		ui->statusBar->showMessage(tr("IP: %1").arg(hostAddress.toString()));
		QAVCodec encoder = QAVCodec::findEncoder(ui->cmbCodec->itemData(ui->cmbCodec->currentIndex()).toInt());
		if (encoder)
		{
			qDebug() << "ui->cmbSampleRate->currentText()=" << ui->cmbSampleRate->currentText();
			qDebug() << "ui->cmbSampleRate->currentIndex()=" << ui->cmbSampleRate->currentIndex();
			mediaStreamer =  new MediaStreamer(inputDevices.at(ui->cmbInputDevice->currentIndex()),
											   encoder, hostAddress.toString(), ui->spbPortTarget->value(),
											   ui->spbPortRtcp->value(),
											   ui->cmbSampleRate->currentText().toInt(),
											   ui->spbBitRate->value());
			qDebug() << "status=" << mediaStreamer->status();
			if (mediaStreamer->status() == MediaStreamer::Error)
				ui->statusBar->showMessage(tr("MediaStreamer error!"));
			else
			{
				QByteArray sdp = mediaStreamer->getSdp();
				if (!sdp.isEmpty())
					qDebug() << "SDP:" << sdp;
				connect(mediaStreamer, SIGNAL(statusChanged(int)), SLOT(onStreamerStatusChanged(int)));
				mediaStreamer->setStatus(MediaStreamer::Running);
			}
		}
		else
			ui->statusBar->showMessage(tr("Codec not found!"));
	}
	else
		ui->statusBar->showMessage(tr("Host lookup error: %1").arg(AHostInfo.errorString()));
}

void MainWindow::onStreamerStatusChanged(int AStatus)
{
	qDebug() << "MainWindow::onStreamerStatusChanged(" << AStatus << ")";
	switch (AStatus)
	{
		case MediaStreamer::Running:
			if (mediaStreamer->type() == MediaStreamer::Record)
			{
				ui->pbRecord->setText(tr("Stop"));
				ui->pbRecord->setEnabled(true);
			}
			else
			{
				ui->pbCast->setText(tr("Stop"));
				ui->pbCast->setEnabled(true);
			}
			break;
		case MediaStreamer::Error:
			QMessageBox::warning(this, tr("Error"),
								 tr(mediaStreamer->type() == MediaStreamer::Record?
										"Failed to record":"Failed to write")
								 , QMessageBox::Ok);
		case MediaStreamer::Stopped:
			mediaFile->close();
			mediaStreamer->deleteLater();
			if (mediaStreamer->type() == MediaStreamer::Record)
				ui->pbRecord->setText(tr("Record"));
			else
				ui->pbCast->setText(tr("Cast"));
			checkRecord();
			checkPlay();
			checkCast();
			break;
	}
}

void MainWindow::onPlayerStatusChanged(int AStatusNew, int AStatusOld)
{
	qDebug() << "MainWindow::onPlayerStatusChanged(" << AStatusNew << "," << AStatusOld << ")";
	switch (AStatusNew)
	{
		case MediaPlayer::Running:
			ui->pbPlay->setText(tr("Stop"));
			ui->pbPlay->setEnabled(true);
			break;
		case MediaPlayer::Error:
			QMessageBox::warning(this, tr("Error"), tr("Failed to play"), QMessageBox::Ok);
			break;
		case MediaPlayer::Finished:
			mediaFile->close();
			mediaPlayer->deleteLater();
			ui->pbPlay->setText(tr("Play"));
			checkRecord();
			checkPlay();
			checkCast();
			break;
	}
}

void MainWindow::onTargetHostChanged(const QString &AHostName)
{
	Q_UNUSED(AHostName)
	checkCast();
}

void MainWindow::onTargetPortChanged(int APort)
{
	ui->spbPortRtcp->setValue(APort+1);
}

void MainWindow::onPayloadTypeEditTextChanged(const QString &AText)
{
	bool ok;
	int id = AText.toInt(&ok);
	if (ok)
	{
		QPayloadType pt = QPayloadType::staticPayloadType(id);
		if (pt.isValid() && pt.media.testFlag(QPayloadType::Audio))
		{
			int codecId = QPayloadType::idByName(pt.name);
			if (codecId)
			{
				int index = ui->cmbCodec->findData(codecId);
				if (index!=-1)
					ui->cmbCodec->setCurrentIndex(index);
			}

			ui->cmbSampleRate->setEditText(QString::number(pt.clockrate));
			ui->cmbChannels->setCurrentIndex(pt.channels-1);
		}
	}
}
