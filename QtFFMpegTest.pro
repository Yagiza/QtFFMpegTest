#-------------------------------------------------
#
# Project created by QtCreator 2016-05-13T17:14:18
#
#-------------------------------------------------

QT       += core gui multimedia network

greaterThan(QT_MAJOR_VERSION, 4){
	QT += widgets qpffmpeg
	CONFIG += c++11
} else {
	CONFIG += qpffmpeg
	QMAKE_CXXFLAGS += -std=c++11
}

TARGET = QtFFMpegTest
TEMPLATE = app

SOURCES += main.cpp\
           mainwindow.cpp

HEADERS += mainwindow.h

FORMS   += mainwindow.ui
