#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPointer>
#include <QFile>
#include <MediaPlayer>
#include <MediaStreamer>
#include <QHostInfo>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

protected:
	QAVOutputFormat formatByName(const QString &AName) const;
	void checkRtp();
	void checkCast();
	void checkPlay();
	void checkRecord();
protected slots:
	void onFileSelect();
	void onRecord();
	void onCast();
	void onPlay();
	void onDeviceComboBoxIndexChanged(int AIndex);
	void onOutputFormatComboBoxIndexChanged(int AIndex);
	void onOutputFormatComboBoxActivated(int AIndex);
	void onInputFormatComboBoxIndexChanged(int AIndex);
	void onInputFormatComboBoxActivated(int AIndex);
	void onCodecComboBoxIndexChanged(int AIndex);
	void onHostLookupFinished(const QHostInfo &AHostInfo);
	void onStreamerStatusChanged(int AStatus);
	void onPlayerStatusChanged(int AStatusNew, int AStatusOld);
	void onTargetHostChanged(const QString &AHostName);
	void onTargetPortChanged(int APort);
	void onPayloadTypeEditTextChanged(const QString &AText);

private:
	Ui::MainWindow *ui;
	QPointer<MediaStreamer> mediaStreamer;
	QPointer<MediaPlayer> mediaPlayer;
	QList<QAudioDeviceInfo> inputDevices;
	QList<QAudioDeviceInfo> outputDevices;
	QHostAddress		hostAddress;
	QFile *mediaFile;

	QUdpSocket *rtp;
	QUdpSocket *rtcp;
};

#endif // MAINWINDOW_H
